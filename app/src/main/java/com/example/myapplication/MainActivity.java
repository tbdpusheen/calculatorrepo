package com.example.myapplication;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.lang.reflect.Array;

@SuppressWarnings({"ALL", "ConstantConditions"})
public class MainActivity extends AppCompatActivity {

    // Variables
    String currentNumberString;
    String currentCalculationsString;
    String previousNumberString;
    String pendingOperation = null;
    ImageButton pendingOperationButton;
    TextView pendingOperationButtonText;
    double previousNumber;
    boolean currentNumberHasDecimal = false;
    boolean clearDisplayNextDigit = true;

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Calculator display variables
        TextView displayTextView = findViewById(R.id.display);
        TextView currentCalculationsTextView = findViewById(R.id.current_calculations);

        //region Calculator Button Variables
        ImageButton allClearButton = findViewById(R.id.allClear);
        ImageButton changeSignButton = findViewById(R.id.changeSign);
        ImageButton percentButton = findViewById(R.id.percent);
        ImageButton divideButton = findViewById(R.id.divide);
        ImageButton multiplyButton = findViewById(R.id.multiply);
        ImageButton subtractButton = findViewById(R.id.subtract);
        ImageButton additionButton = findViewById(R.id.addition);
        ImageButton equalsButton = findViewById(R.id.equals);
        ImageButton nineButton = findViewById(R.id.nine);
        ImageButton eightButton = findViewById(R.id.eight);
        ImageButton sevenButton = findViewById(R.id.seven);
        ImageButton sixButton = findViewById(R.id.six);
        ImageButton fiveButton = findViewById(R.id.five);
        ImageButton fourButton = findViewById(R.id.four);
        ImageButton threeButton = findViewById(R.id.three);
        ImageButton twoButton = findViewById(R.id.two);
        ImageButton oneButton = findViewById(R.id.one);
        ImageButton zeroButton = findViewById(R.id.zero);
        ImageButton decimalButton = findViewById(R.id.decimal);
        TextView divideButtonText = findViewById(R.id.divide_text);
        TextView multiplyButtonText = findViewById(R.id.multiply_text);
        TextView subtractButtonText = findViewById(R.id.subtract_text);
        TextView additionButtonText = findViewById(R.id.addition_text);
        //endregion

        allClearButton.setOnClickListener(v -> {

            // Clears out all variables and the current displays
            currentNumberString = "0";
            currentCalculationsString = "";
            previousNumber = 0.0;
            previousNumberString = null;
            pendingOperation = null;
            currentNumberHasDecimal = false;
            clearDisplayNextDigit = true;
            displayTextView.setText(currentNumberString);
            currentCalculationsTextView.setText(currentCalculationsString);
            System.out.println("Display and calculations cleared");

            // Release the pressed operation button if one was pressed
            if (pendingOperationButton != null) {
                pendingOperationButton.setVisibility(View.VISIBLE);
                pendingOperationButtonText.setVisibility(View.VISIBLE);
                System.out.println(pendingOperationButton.toString() + " and its text made visible");
            }

            System.out.println("AC was tapped");
        });

        changeSignButton.setOnClickListener(v -> {

            // If the current number is negative already
            if (currentNumberString.contains("-")) {
                // Remove the negative symbol from the string
                currentNumberString = currentNumberString.replace("-", "");
            }
            else {
                // Make the current string negative
                currentNumberString = "-" + currentNumberString;
            }

            // Set the display with the updated string
            displayTextView.setText(currentNumberString);
            System.out.println("Display is " + currentNumberString);

            System.out.println("Change sign was tapped");
        });

        percentButton.setOnClickListener(v -> {

            // Convert current number to a double
            double currentNumber = Double.parseDouble(currentNumberString);

            // If the current operation is addition or subtraction, set the current number to the percent of the previous number
            if ((pendingOperation != null) ) {
                if (pendingOperation.equals("Add") || pendingOperation.equals("Subtract")) {
                    System.out.println("previous number is " + previousNumber);
                    currentNumber = previousNumber * (currentNumber/100);
                }
                // Else, change the number to decimal/percent form
                else {
                    currentNumber /= 100;
                }
            }
            // Else, change the number to decimal/percent form
            else {
                currentNumber /= 100;
            }

            // Replace the current number with the results of the percent configuration
            currentNumberString = Double.valueOf(currentNumber).toString();

            // If the current string ends with a decimal and zero, remove it
            if (currentNumberString.endsWith(".0")) {
                currentNumberString = currentNumberString.replace(".0", "");
            }

            // Set the display number as the current number
            displayTextView.setText(currentNumberString);
            System.out.println("Display is " + currentNumberString);

            System.out.println("Percent was tapped");
        });

        divideButton.setOnClickListener(v -> {

            // If there was already a pending operation
            if (pendingOperation != null && !clearDisplayNextDigit) {
                // Convert current number to a double
                double currentNumber = Double.parseDouble(currentNumberString);

                // Perform the pending operation on the number if it exists
                switch (pendingOperation) {
                    case "Add":
                        previousNumber += currentNumber;

                        // Set the current calculations display
                        // If the combination of both inputs is larger than the display can handle, convert the smallest into "..." if the other is not too large
                        if (((previousNumberString.length() + currentNumberString.length()) > 13) && !((previousNumberString.length() > 10)) && (currentNumberString.length() > 10)) {
                            // If previousNumberString is the largest, convert it into "..."
                            if (previousNumberString.length() > currentNumberString.length()) {
                                currentCalculationsString = previousNumberString + " + ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the largest, convert it into "..."
                            else if (currentNumberString.length() > previousNumberString.length()) {
                                currentCalculationsString = "... + " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If they are the same length, convert previousNumberString by default
                            else {
                                currentCalculationsString = previousNumberString + " + ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the combination of both inputs is larger than the display can handle and either of the strings singlehandedly exceed the display, convert any that do into "..."
                        else if (previousNumberString.length() > 10 || currentNumberString.length() > 10) {
                            // If both exceed the limit, convert them both into "..."
                            if (previousNumberString.length() > 13 && currentNumberString.length() > 13) {
                                currentCalculationsString = "... + ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                            // If previousNumberString is the one that exceeds the limit, convert it into "...", but only if the currentNumberString doesn't exceed the limit after adding "..."
                            else if (previousNumberString.length() > 13 && !(currentNumberString.length() > 10)) {
                                currentCalculationsString = "... + " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the one that exceeds the limit, convert it into "...", but only if the previousNumberString doesn't exceed the limit after adding "..."
                            else if (currentNumberString.length() > 13 && !(previousNumberString.length() > 10)) {
                                currentCalculationsString = previousNumberString + " + ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If only one of the numbers are over the display limit but the other is over the limit if we include the added "...", convert them both into "..." anyways
                            else {
                                currentCalculationsString = "... + ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the string is displayable, set the current calculations string as the numbers entered
                        else {
                            currentCalculationsString = previousNumberString + " + " + currentNumberString + " =";
                        }

                        break;
                    case "Subtract":
                        previousNumber -= currentNumber;

                        // Sets the current calculations display
                        // If the combination of both inputs is larger than the display can handle, convert the smallest into "..." if the other is not too large
                        if (((previousNumberString.length() + currentNumberString.length()) > 13) && !((previousNumberString.length() > 10)) && (currentNumberString.length() > 10)) {
                            // If previousNumberString is the largest, convert it into "..."
                            if (previousNumberString.length() > currentNumberString.length()) {
                                currentCalculationsString = previousNumberString + " − ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the largest, convert it into "..."
                            else if (currentNumberString.length() > previousNumberString.length()) {
                                currentCalculationsString = "... − " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If they are the same length, convert previousNumberString by default
                            else {
                                currentCalculationsString = previousNumberString + " + ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the combination of both inputs is larger than the display can handle and either of the strings singlehandedly exceed the display, convert any that do into "..."
                        else if (previousNumberString.length() > 10 || currentNumberString.length() > 10) {
                            // If both exceed the limit, convert them both into "..."
                            if (previousNumberString.length() > 13 && currentNumberString.length() > 13) {
                                currentCalculationsString = "... − ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                            // If previousNumberString is the one that exceeds the limit, convert it into "...", but only if the currentNumberString doesn't exceed the limit after adding "..."
                            else if (previousNumberString.length() > 13 && !(currentNumberString.length() > 10)) {
                                currentCalculationsString = "... − " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the one that exceeds the limit, convert it into "...", but only if the previousNumberString doesn't exceed the limit after adding "..."
                            else if (currentNumberString.length() > 13 && !(previousNumberString.length() > 10)) {
                                currentCalculationsString = previousNumberString + " − ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If only one of the numbers are over the display limit but the other is over the limit if we include the added "...", convert them both into "..." anyways
                            else {
                                currentCalculationsString = "... − ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the string is displayable, set the current calculations string as the numbers entered
                        else {
                            currentCalculationsString = previousNumberString + " − " + currentNumberString + " =";
                        }

                        break;
                    case "Multiply":
                        previousNumber *= currentNumber;

                        // Sets the current calculations display
                        // If the combination of both inputs is larger than the display can handle, convert the smallest into "..." if the other is not too large
                        if (((previousNumberString.length() + currentNumberString.length()) > 13) && !((previousNumberString.length() > 10)) && (currentNumberString.length() > 10)) {
                            // If previousNumberString is the largest, convert it into "..."
                            if (previousNumberString.length() > currentNumberString.length()) {
                                currentCalculationsString = previousNumberString + " × ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the largest, convert it into "..."
                            else if (currentNumberString.length() > previousNumberString.length()) {
                                currentCalculationsString = "... × " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If they are the same length, convert previousNumberString by default
                            else {
                                currentCalculationsString = previousNumberString + " + ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the combination of both inputs is larger than the display can handle and either of the strings singlehandedly exceed the display, convert any that do into "..."
                        else if (previousNumberString.length() > 10 || currentNumberString.length() > 10) {
                            // If both exceed the limit, convert them both into "..."
                            if (previousNumberString.length() > 13 && currentNumberString.length() > 13) {
                                currentCalculationsString = "... × ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                            // If previousNumberString is the one that exceeds the limit, convert it into "...", but only if the currentNumberString doesn't exceed the limit after adding "..."
                            else if (previousNumberString.length() > 13 && !(currentNumberString.length() > 10)) {
                                currentCalculationsString = "... × " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the one that exceeds the limit, convert it into "...", but only if the previousNumberString doesn't exceed the limit after adding "..."
                            else if (currentNumberString.length() > 13 && !(previousNumberString.length() > 10)) {
                                currentCalculationsString = previousNumberString + " × ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If only one of the numbers are over the display limit but the other is over the limit if we include the added "...", convert them both into "..." anyways
                            else {
                                currentCalculationsString = "... × ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the string is displayable, set the current calculations string as the numbers entered
                        else {
                            currentCalculationsString = previousNumberString + " × " + currentNumberString + " =";
                        }

                        break;
                    case "Divide":
                        previousNumber /= currentNumber;

                        // Sets the current calculations display
                        // If the combination of both inputs is larger than the display can handle, convert the smallest into "..." if the other is not too large
                        if (((previousNumberString.length() + currentNumberString.length()) > 13) && !((previousNumberString.length() > 10)) && (currentNumberString.length() > 10)) {
                            // If previousNumberString is the largest, convert it into "..."
                            if (previousNumberString.length() > currentNumberString.length()) {
                                currentCalculationsString = previousNumberString + " ÷ ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the largest, convert it into "..."
                            else if (currentNumberString.length() > previousNumberString.length()) {
                                currentCalculationsString = "... ÷ " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If they are the same length, convert previousNumberString by default
                            else {
                                currentCalculationsString = previousNumberString + " + ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the combination of both inputs is larger than the display can handle and either of the strings singlehandedly exceed the display, convert any that do into "..."
                        else if (previousNumberString.length() > 10 || currentNumberString.length() > 10) {
                            // If both exceed the limit, convert them both into "..."
                            if (previousNumberString.length() > 13 && currentNumberString.length() > 13) {
                                currentCalculationsString = "... ÷ ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                            // If previousNumberString is the one that exceeds the limit, convert it into "...", but only if the currentNumberString doesn't exceed the limit after adding "..."
                            else if (previousNumberString.length() > 13 && !(currentNumberString.length() > 10)) {
                                currentCalculationsString = "... ÷ " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the one that exceeds the limit, convert it into "...", but only if the previousNumberString doesn't exceed the limit after adding "..."
                            else if (currentNumberString.length() > 13 && !(previousNumberString.length() > 10)) {
                                currentCalculationsString = previousNumberString + " ÷ ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If only one of the numbers are over the display limit but the other is over the limit if we include the added "...", convert them both into "..." anyways
                            else {
                                currentCalculationsString = "... ÷ ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the string is displayable, set the current calculations string as the numbers entered
                        else {
                            currentCalculationsString = previousNumberString + " ÷ " + currentNumberString + " =";
                        }

                        break;
                }

                // Replace the current number with the results of the pending operation
                currentNumberString = Double.valueOf(previousNumber).toString();

                // If the current string ends with a decimal and zero, remove it
                if (currentNumberString.endsWith(".0")) {
                    currentNumberString = currentNumberString.replace(".0", "");
                }

                // Set the display number as the current number
                displayTextView.setText(currentNumberString);
                System.out.println("Display is " + currentNumberString);

                // Set the current calculations display to the entered operation
                currentCalculationsTextView.setText(currentCalculationsString);
                System.out.println("Current calculations are " + currentCalculationsString);
            }
            // If there was a pending operation but the user has not input another number, release the old pending operation
            else if (pendingOperation != null) {
                // Release the pressed operation button
                if (pendingOperationButton != null) {
                    pendingOperationButton.setVisibility(View.VISIBLE);
                    pendingOperationButtonText.setVisibility(View.VISIBLE);
                    System.out.println(pendingOperationButton.toString() + " and its text made visible");
                }
            }

            // Save the current number as the previous number
            previousNumber = Double.parseDouble(currentNumberString);

            // Set the previous number string to the current number for future reference
            previousNumberString = currentNumberString;

            // Set the pending operation as the selected operation
            pendingOperation = "Divide";

            // Sets the current pressed operation as the selected operation
            pendingOperationButton = divideButton;
            pendingOperationButtonText = divideButtonText;
            pendingOperationButton.setVisibility(View.INVISIBLE);
            pendingOperationButtonText.setVisibility(View.INVISIBLE);

            // Clear the display on the next digit entered
            clearDisplayNextDigit = true;

            System.out.println("Divide was tapped");
        });

        multiplyButton.setOnClickListener(v -> {

            // If there was already a pending operation
            if (pendingOperation != null && !clearDisplayNextDigit) {
                // Convert current number to a double
                double currentNumber = Double.parseDouble(currentNumberString);

                // Perform the pending operation on the number if it exists
                switch (pendingOperation) {
                    case "Add":
                        previousNumber += currentNumber;

                        // Set the current calculations display
                        // If the combination of both inputs is larger than the display can handle, convert the smallest into "..." if the other is not too large
                        if (((previousNumberString.length() + currentNumberString.length()) > 13) && !((previousNumberString.length() > 10)) && (currentNumberString.length() > 10)) {
                            // If previousNumberString is the largest, convert it into "..."
                            if (previousNumberString.length() > currentNumberString.length()) {
                                currentCalculationsString = previousNumberString + " + ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the largest, convert it into "..."
                            else if (currentNumberString.length() > previousNumberString.length()) {
                                currentCalculationsString = "... + " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If they are the same length, convert previousNumberString by default
                            else {
                                currentCalculationsString = previousNumberString + " + ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the combination of both inputs is larger than the display can handle and either of the strings singlehandedly exceed the display, convert any that do into "..."
                        else if (previousNumberString.length() > 10 || currentNumberString.length() > 10) {
                            // If both exceed the limit, convert them both into "..."
                            if (previousNumberString.length() > 13 && currentNumberString.length() > 13) {
                                currentCalculationsString = "... + ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                            // If previousNumberString is the one that exceeds the limit, convert it into "...", but only if the currentNumberString doesn't exceed the limit after adding "..."
                            else if (previousNumberString.length() > 13 && !(currentNumberString.length() > 10)) {
                                currentCalculationsString = "... + " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the one that exceeds the limit, convert it into "...", but only if the previousNumberString doesn't exceed the limit after adding "..."
                            else if (currentNumberString.length() > 13 && !(previousNumberString.length() > 10)) {
                                currentCalculationsString = previousNumberString + " + ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If only one of the numbers are over the display limit but the other is over the limit if we include the added "...", convert them both into "..." anyways
                            else {
                                currentCalculationsString = "... + ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the string is displayable, set the current calculations string as the numbers entered
                        else {
                            currentCalculationsString = previousNumberString + " + " + currentNumberString + " =";
                        }

                        break;
                    case "Subtract":
                        previousNumber -= currentNumber;

                        // Sets the current calculations display
                        // If the combination of both inputs is larger than the display can handle, convert the smallest into "..." if the other is not too large
                        if (((previousNumberString.length() + currentNumberString.length()) > 13) && !((previousNumberString.length() > 10)) && (currentNumberString.length() > 10)) {
                            // If previousNumberString is the largest, convert it into "..."
                            if (previousNumberString.length() > currentNumberString.length()) {
                                currentCalculationsString = previousNumberString + " − ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the largest, convert it into "..."
                            else if (currentNumberString.length() > previousNumberString.length()) {
                                currentCalculationsString = "... − " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If they are the same length, convert previousNumberString by default
                            else {
                                currentCalculationsString = previousNumberString + " + ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the combination of both inputs is larger than the display can handle and either of the strings singlehandedly exceed the display, convert any that do into "..."
                        else if (previousNumberString.length() > 10 || currentNumberString.length() > 10) {
                            // If both exceed the limit, convert them both into "..."
                            if (previousNumberString.length() > 13 && currentNumberString.length() > 13) {
                                currentCalculationsString = "... − ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                            // If previousNumberString is the one that exceeds the limit, convert it into "...", but only if the currentNumberString doesn't exceed the limit after adding "..."
                            else if (previousNumberString.length() > 13 && !(currentNumberString.length() > 10)) {
                                currentCalculationsString = "... − " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the one that exceeds the limit, convert it into "...", but only if the previousNumberString doesn't exceed the limit after adding "..."
                            else if (currentNumberString.length() > 13 && !(previousNumberString.length() > 10)) {
                                currentCalculationsString = previousNumberString + " − ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If only one of the numbers are over the display limit but the other is over the limit if we include the added "...", convert them both into "..." anyways
                            else {
                                currentCalculationsString = "... − ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the string is displayable, set the current calculations string as the numbers entered
                        else {
                            currentCalculationsString = previousNumberString + " − " + currentNumberString + " =";
                        }

                        break;
                    case "Multiply":
                        previousNumber *= currentNumber;

                        // Sets the current calculations display
                        // If the combination of both inputs is larger than the display can handle, convert the smallest into "..." if the other is not too large
                        if (((previousNumberString.length() + currentNumberString.length()) > 13) && !((previousNumberString.length() > 10)) && (currentNumberString.length() > 10)) {
                            // If previousNumberString is the largest, convert it into "..."
                            if (previousNumberString.length() > currentNumberString.length()) {
                                currentCalculationsString = previousNumberString + " × ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the largest, convert it into "..."
                            else if (currentNumberString.length() > previousNumberString.length()) {
                                currentCalculationsString = "... × " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If they are the same length, convert previousNumberString by default
                            else {
                                currentCalculationsString = previousNumberString + " + ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the combination of both inputs is larger than the display can handle and either of the strings singlehandedly exceed the display, convert any that do into "..."
                        else if (previousNumberString.length() > 10 || currentNumberString.length() > 10) {
                            // If both exceed the limit, convert them both into "..."
                            if (previousNumberString.length() > 13 && currentNumberString.length() > 13) {
                                currentCalculationsString = "... × ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                            // If previousNumberString is the one that exceeds the limit, convert it into "...", but only if the currentNumberString doesn't exceed the limit after adding "..."
                            else if (previousNumberString.length() > 13 && !(currentNumberString.length() > 10)) {
                                currentCalculationsString = "... × " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the one that exceeds the limit, convert it into "...", but only if the previousNumberString doesn't exceed the limit after adding "..."
                            else if (currentNumberString.length() > 13 && !(previousNumberString.length() > 10)) {
                                currentCalculationsString = previousNumberString + " × ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If only one of the numbers are over the display limit but the other is over the limit if we include the added "...", convert them both into "..." anyways
                            else {
                                currentCalculationsString = "... × ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the string is displayable, set the current calculations string as the numbers entered
                        else {
                            currentCalculationsString = previousNumberString + " × " + currentNumberString + " =";
                        }

                        break;
                    case "Divide":
                        previousNumber /= currentNumber;

                        // Sets the current calculations display
                        // If the combination of both inputs is larger than the display can handle, convert the smallest into "..." if the other is not too large
                        if (((previousNumberString.length() + currentNumberString.length()) > 13) && !((previousNumberString.length() > 10)) && (currentNumberString.length() > 10)) {
                            // If previousNumberString is the largest, convert it into "..."
                            if (previousNumberString.length() > currentNumberString.length()) {
                                currentCalculationsString = previousNumberString + " ÷ ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the largest, convert it into "..."
                            else if (currentNumberString.length() > previousNumberString.length()) {
                                currentCalculationsString = "... ÷ " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If they are the same length, convert previousNumberString by default
                            else {
                                currentCalculationsString = previousNumberString + " + ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the combination of both inputs is larger than the display can handle and either of the strings singlehandedly exceed the display, convert any that do into "..."
                        else if (previousNumberString.length() > 10 || currentNumberString.length() > 10) {
                            // If both exceed the limit, convert them both into "..."
                            if (previousNumberString.length() > 13 && currentNumberString.length() > 13) {
                                currentCalculationsString = "... ÷ ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                            // If previousNumberString is the one that exceeds the limit, convert it into "...", but only if the currentNumberString doesn't exceed the limit after adding "..."
                            else if (previousNumberString.length() > 13 && !(currentNumberString.length() > 10)) {
                                currentCalculationsString = "... ÷ " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the one that exceeds the limit, convert it into "...", but only if the previousNumberString doesn't exceed the limit after adding "..."
                            else if (currentNumberString.length() > 13 && !(previousNumberString.length() > 10)) {
                                currentCalculationsString = previousNumberString + " ÷ ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If only one of the numbers are over the display limit but the other is over the limit if we include the added "...", convert them both into "..." anyways
                            else {
                                currentCalculationsString = "... ÷ ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the string is displayable, set the current calculations string as the numbers entered
                        else {
                            currentCalculationsString = previousNumberString + " ÷ " + currentNumberString + " =";
                        }

                        break;
                }

                // Replace the current number with the results of the pending operation
                currentNumberString = Double.valueOf(previousNumber).toString();

                // If the current string ends with a decimal and zero, remove it
                if (currentNumberString.endsWith(".0")) {
                    currentNumberString = currentNumberString.replace(".0", "");
                }

                // Set the display number as the current number
                displayTextView.setText(currentNumberString);
                System.out.println("Display is " + currentNumberString);

                // Set the current calculations display to the entered operation
                currentCalculationsTextView.setText(currentCalculationsString);
                System.out.println("Current calculations are " + currentCalculationsString);
            }
            // If there was a pending operation but the user has not input another number, release the old pending operation
            else if (pendingOperation != null) {
                // Release the pressed operation button
                if (pendingOperationButton != null) {
                    pendingOperationButton.setVisibility(View.VISIBLE);
                    pendingOperationButtonText.setVisibility(View.VISIBLE);
                    System.out.println(pendingOperationButton.toString() + " and its text made visible");
                }
            }

            // Save the current number as the previous number
            previousNumber = Double.parseDouble(currentNumberString);

            // Set the previous number string to the current number for future reference
            previousNumberString = currentNumberString;

            // Sets the current pressed operation as the selected operation
            pendingOperationButton = multiplyButton;
            pendingOperationButtonText = multiplyButtonText;
            pendingOperationButton.setVisibility(View.INVISIBLE);
            pendingOperationButtonText.setVisibility(View.INVISIBLE);

            // Set the pending operation as the selected operation
            pendingOperation = "Multiply";

            // Clear the display on the next digit entered
            clearDisplayNextDigit = true;

            System.out.println("Multiply was tapped");
        });

        subtractButton.setOnClickListener(v -> {

            // If there was already a pending operation
            if (pendingOperation != null && !clearDisplayNextDigit) {
                // Convert current number to a double
                double currentNumber = Double.parseDouble(currentNumberString);

                // Perform the pending operation on the number if it exists
                switch (pendingOperation) {
                    case "Add":
                        previousNumber += currentNumber;

                        // Set the current calculations display
                        // If the combination of both inputs is larger than the display can handle, convert the smallest into "..." if the other is not too large
                        if (((previousNumberString.length() + currentNumberString.length()) > 13) && !((previousNumberString.length() > 10)) && (currentNumberString.length() > 10)) {
                            // If previousNumberString is the largest, convert it into "..."
                            if (previousNumberString.length() > currentNumberString.length()) {
                                currentCalculationsString = previousNumberString + " + ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the largest, convert it into "..."
                            else if (currentNumberString.length() > previousNumberString.length()) {
                                currentCalculationsString = "... + " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If they are the same length, convert previousNumberString by default
                            else {
                                currentCalculationsString = previousNumberString + " + ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the combination of both inputs is larger than the display can handle and either of the strings singlehandedly exceed the display, convert any that do into "..."
                        else if (previousNumberString.length() > 10 || currentNumberString.length() > 10) {
                            // If both exceed the limit, convert them both into "..."
                            if (previousNumberString.length() > 13 && currentNumberString.length() > 13) {
                                currentCalculationsString = "... + ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                            // If previousNumberString is the one that exceeds the limit, convert it into "...", but only if the currentNumberString doesn't exceed the limit after adding "..."
                            else if (previousNumberString.length() > 13 && !(currentNumberString.length() > 10)) {
                                currentCalculationsString = "... + " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the one that exceeds the limit, convert it into "...", but only if the previousNumberString doesn't exceed the limit after adding "..."
                            else if (currentNumberString.length() > 13 && !(previousNumberString.length() > 10)) {
                                currentCalculationsString = previousNumberString + " + ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If only one of the numbers are over the display limit but the other is over the limit if we include the added "...", convert them both into "..." anyways
                            else {
                                currentCalculationsString = "... + ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the string is displayable, set the current calculations string as the numbers entered
                        else {
                            currentCalculationsString = previousNumberString + " + " + currentNumberString + " =";
                        }

                        break;
                    case "Subtract":
                        previousNumber -= currentNumber;

                        // Sets the current calculations display
                        // If the combination of both inputs is larger than the display can handle, convert the smallest into "..." if the other is not too large
                        if (((previousNumberString.length() + currentNumberString.length()) > 13) && !((previousNumberString.length() > 10)) && (currentNumberString.length() > 10)) {
                            // If previousNumberString is the largest, convert it into "..."
                            if (previousNumberString.length() > currentNumberString.length()) {
                                currentCalculationsString = previousNumberString + " − ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the largest, convert it into "..."
                            else if (currentNumberString.length() > previousNumberString.length()) {
                                currentCalculationsString = "... − " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If they are the same length, convert previousNumberString by default
                            else {
                                currentCalculationsString = previousNumberString + " + ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the combination of both inputs is larger than the display can handle and either of the strings singlehandedly exceed the display, convert any that do into "..."
                        else if (previousNumberString.length() > 10 || currentNumberString.length() > 10) {
                            // If both exceed the limit, convert them both into "..."
                            if (previousNumberString.length() > 13 && currentNumberString.length() > 13) {
                                currentCalculationsString = "... − ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                            // If previousNumberString is the one that exceeds the limit, convert it into "...", but only if the currentNumberString doesn't exceed the limit after adding "..."
                            else if (previousNumberString.length() > 13 && !(currentNumberString.length() > 10)) {
                                currentCalculationsString = "... − " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the one that exceeds the limit, convert it into "...", but only if the previousNumberString doesn't exceed the limit after adding "..."
                            else if (currentNumberString.length() > 13 && !(previousNumberString.length() > 10)) {
                                currentCalculationsString = previousNumberString + " − ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If only one of the numbers are over the display limit but the other is over the limit if we include the added "...", convert them both into "..." anyways
                            else {
                                currentCalculationsString = "... − ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the string is displayable, set the current calculations string as the numbers entered
                        else {
                            currentCalculationsString = previousNumberString + " − " + currentNumberString + " =";
                        }

                        break;
                    case "Multiply":
                        previousNumber *= currentNumber;

                        // Sets the current calculations display
                        // If the combination of both inputs is larger than the display can handle, convert the smallest into "..." if the other is not too large
                        if (((previousNumberString.length() + currentNumberString.length()) > 13) && !((previousNumberString.length() > 10)) && (currentNumberString.length() > 10)) {
                            // If previousNumberString is the largest, convert it into "..."
                            if (previousNumberString.length() > currentNumberString.length()) {
                                currentCalculationsString = previousNumberString + " × ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the largest, convert it into "..."
                            else if (currentNumberString.length() > previousNumberString.length()) {
                                currentCalculationsString = "... × " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If they are the same length, convert previousNumberString by default
                            else {
                                currentCalculationsString = previousNumberString + " + ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the combination of both inputs is larger than the display can handle and either of the strings singlehandedly exceed the display, convert any that do into "..."
                        else if (previousNumberString.length() > 10 || currentNumberString.length() > 10) {
                            // If both exceed the limit, convert them both into "..."
                            if (previousNumberString.length() > 13 && currentNumberString.length() > 13) {
                                currentCalculationsString = "... × ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                            // If previousNumberString is the one that exceeds the limit, convert it into "...", but only if the currentNumberString doesn't exceed the limit after adding "..."
                            else if (previousNumberString.length() > 13 && !(currentNumberString.length() > 10)) {
                                currentCalculationsString = "... × " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the one that exceeds the limit, convert it into "...", but only if the previousNumberString doesn't exceed the limit after adding "..."
                            else if (currentNumberString.length() > 13 && !(previousNumberString.length() > 10)) {
                                currentCalculationsString = previousNumberString + " × ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If only one of the numbers are over the display limit but the other is over the limit if we include the added "...", convert them both into "..." anyways
                            else {
                                currentCalculationsString = "... × ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the string is displayable, set the current calculations string as the numbers entered
                        else {
                            currentCalculationsString = previousNumberString + " × " + currentNumberString + " =";
                        }

                        break;
                    case "Divide":
                        previousNumber /= currentNumber;

                        // Sets the current calculations display
                        // If the combination of both inputs is larger than the display can handle, convert the smallest into "..." if the other is not too large
                        if (((previousNumberString.length() + currentNumberString.length()) > 13) && !((previousNumberString.length() > 10)) && (currentNumberString.length() > 10)) {
                            // If previousNumberString is the largest, convert it into "..."
                            if (previousNumberString.length() > currentNumberString.length()) {
                                currentCalculationsString = previousNumberString + " ÷ ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the largest, convert it into "..."
                            else if (currentNumberString.length() > previousNumberString.length()) {
                                currentCalculationsString = "... ÷ " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If they are the same length, convert previousNumberString by default
                            else {
                                currentCalculationsString = previousNumberString + " + ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the combination of both inputs is larger than the display can handle and either of the strings singlehandedly exceed the display, convert any that do into "..."
                        else if (previousNumberString.length() > 10 || currentNumberString.length() > 10) {
                            // If both exceed the limit, convert them both into "..."
                            if (previousNumberString.length() > 13 && currentNumberString.length() > 13) {
                                currentCalculationsString = "... ÷ ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                            // If previousNumberString is the one that exceeds the limit, convert it into "...", but only if the currentNumberString doesn't exceed the limit after adding "..."
                            else if (previousNumberString.length() > 13 && !(currentNumberString.length() > 10)) {
                                currentCalculationsString = "... ÷ " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the one that exceeds the limit, convert it into "...", but only if the previousNumberString doesn't exceed the limit after adding "..."
                            else if (currentNumberString.length() > 13 && !(previousNumberString.length() > 10)) {
                                currentCalculationsString = previousNumberString + " ÷ ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If only one of the numbers are over the display limit but the other is over the limit if we include the added "...", convert them both into "..." anyways
                            else {
                                currentCalculationsString = "... ÷ ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the string is displayable, set the current calculations string as the numbers entered
                        else {
                            currentCalculationsString = previousNumberString + " ÷ " + currentNumberString + " =";
                        }

                        break;
                }

                // Replace the current number with the results of the pending operation
                currentNumberString = Double.valueOf(previousNumber).toString();

                // If the current string ends with a decimal and zero, remove it
                if (currentNumberString.endsWith(".0")) {
                    currentNumberString = currentNumberString.replace(".0", "");
                }

                // Set the display number as the current number
                displayTextView.setText(currentNumberString);
                System.out.println("Display is " + currentNumberString);

                // Set the current calculations display to the entered operation
                currentCalculationsTextView.setText(currentCalculationsString);
                System.out.println("Current calculations are " + currentCalculationsString);
            }
            // If there was a pending operation but the user has not input another number, release the old pending operation
            else if (pendingOperation != null) {
                // Release the pressed operation button
                if (pendingOperationButton != null) {
                    pendingOperationButton.setVisibility(View.VISIBLE);
                    pendingOperationButtonText.setVisibility(View.VISIBLE);
                    System.out.println(pendingOperationButton.toString() + " and its text made visible");
                }
            }

            // Save the current number as the previous number
            previousNumber = Double.parseDouble(currentNumberString);

            // Set the previous number string to the current number for future reference
            previousNumberString = currentNumberString;

            // Set the pending operation as the selected operation
            pendingOperation = "Subtract";

            // Sets the current pressed operation as the selected operation
            pendingOperationButton = subtractButton;
            pendingOperationButtonText = subtractButtonText;
            pendingOperationButton.setVisibility(View.INVISIBLE);
            pendingOperationButtonText.setVisibility(View.INVISIBLE);

            // Clear the display on the next digit entered
            clearDisplayNextDigit = true;

            System.out.println("Subtract was tapped");
        });

        additionButton.setOnClickListener(v -> {

            // If there is a pending operation
            if (pendingOperation != null && !clearDisplayNextDigit) {
                // Convert current number to a double
                double currentNumber = Double.parseDouble(currentNumberString);

                // Perform the pending operation on the number if it exists
                switch (pendingOperation) {
                    case "Add":
                        previousNumber += currentNumber;

                        // Set the current calculations display
                        // If the combination of both inputs is larger than the display can handle, convert the smallest into "..." if the other is not too large
                        if (((previousNumberString.length() + currentNumberString.length()) > 13) && !((previousNumberString.length() > 10)) && (currentNumberString.length() > 10)) {
                            // If previousNumberString is the largest, convert it into "..."
                            if (previousNumberString.length() > currentNumberString.length()) {
                                currentCalculationsString = previousNumberString + " + ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the largest, convert it into "..."
                            else if (currentNumberString.length() > previousNumberString.length()) {
                                currentCalculationsString = "... + " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If they are the same length, convert previousNumberString by default
                            else {
                                currentCalculationsString = previousNumberString + " + ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the combination of both inputs is larger than the display can handle and either of the strings singlehandedly exceed the display, convert any that do into "..."
                        else if (previousNumberString.length() > 10 || currentNumberString.length() > 10) {
                            // If both exceed the limit, convert them both into "..."
                            if (previousNumberString.length() > 13 && currentNumberString.length() > 13) {
                                currentCalculationsString = "... + ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                            // If previousNumberString is the one that exceeds the limit, convert it into "...", but only if the currentNumberString doesn't exceed the limit after adding "..."
                            else if (previousNumberString.length() > 13 && !(currentNumberString.length() > 10)) {
                                currentCalculationsString = "... + " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the one that exceeds the limit, convert it into "...", but only if the previousNumberString doesn't exceed the limit after adding "..."
                            else if (currentNumberString.length() > 13 && !(previousNumberString.length() > 10)) {
                                currentCalculationsString = previousNumberString + " + ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If only one of the numbers are over the display limit but the other is over the limit if we include the added "...", convert them both into "..." anyways
                            else {
                                currentCalculationsString = "... + ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the string is displayable, set the current calculations string as the numbers entered
                        else {
                            currentCalculationsString = previousNumberString + " + " + currentNumberString + " =";
                        }

                        break;
                    case "Subtract":
                        previousNumber -= currentNumber;

                        // Sets the current calculations display
                        // If the combination of both inputs is larger than the display can handle, convert the smallest into "..." if the other is not too large
                        if (((previousNumberString.length() + currentNumberString.length()) > 13) && !((previousNumberString.length() > 10)) && (currentNumberString.length() > 10)) {
                            // If previousNumberString is the largest, convert it into "..."
                            if (previousNumberString.length() > currentNumberString.length()) {
                                currentCalculationsString = previousNumberString + " − ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the largest, convert it into "..."
                            else if (currentNumberString.length() > previousNumberString.length()) {
                                currentCalculationsString = "... − " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If they are the same length, convert previousNumberString by default
                            else {
                                currentCalculationsString = previousNumberString + " + ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the combination of both inputs is larger than the display can handle and either of the strings singlehandedly exceed the display, convert any that do into "..."
                        else if (previousNumberString.length() > 10 || currentNumberString.length() > 10) {
                            // If both exceed the limit, convert them both into "..."
                            if (previousNumberString.length() > 13 && currentNumberString.length() > 13) {
                                currentCalculationsString = "... − ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                            // If previousNumberString is the one that exceeds the limit, convert it into "...", but only if the currentNumberString doesn't exceed the limit after adding "..."
                            else if (previousNumberString.length() > 13 && !(currentNumberString.length() > 10)) {
                                currentCalculationsString = "... − " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the one that exceeds the limit, convert it into "...", but only if the previousNumberString doesn't exceed the limit after adding "..."
                            else if (currentNumberString.length() > 13 && !(previousNumberString.length() > 10)) {
                                currentCalculationsString = previousNumberString + " − ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If only one of the numbers are over the display limit but the other is over the limit if we include the added "...", convert them both into "..." anyways
                            else {
                                currentCalculationsString = "... − ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the string is displayable, set the current calculations string as the numbers entered
                        else {
                            currentCalculationsString = previousNumberString + " − " + currentNumberString + " =";
                        }

                        break;
                    case "Multiply":
                        previousNumber *= currentNumber;

                        // Sets the current calculations display
                        // If the combination of both inputs is larger than the display can handle, convert the smallest into "..." if the other is not too large
                        if (((previousNumberString.length() + currentNumberString.length()) > 13) && !((previousNumberString.length() > 10)) && (currentNumberString.length() > 10)) {
                            // If previousNumberString is the largest, convert it into "..."
                            if (previousNumberString.length() > currentNumberString.length()) {
                                currentCalculationsString = previousNumberString + " × ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the largest, convert it into "..."
                            else if (currentNumberString.length() > previousNumberString.length()) {
                                currentCalculationsString = "... × " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If they are the same length, convert previousNumberString by default
                            else {
                                currentCalculationsString = previousNumberString + " + ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the combination of both inputs is larger than the display can handle and either of the strings singlehandedly exceed the display, convert any that do into "..."
                        else if (previousNumberString.length() > 10 || currentNumberString.length() > 10) {
                            // If both exceed the limit, convert them both into "..."
                            if (previousNumberString.length() > 13 && currentNumberString.length() > 13) {
                                currentCalculationsString = "... × ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                            // If previousNumberString is the one that exceeds the limit, convert it into "...", but only if the currentNumberString doesn't exceed the limit after adding "..."
                            else if (previousNumberString.length() > 13 && !(currentNumberString.length() > 10)) {
                                currentCalculationsString = "... × " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the one that exceeds the limit, convert it into "...", but only if the previousNumberString doesn't exceed the limit after adding "..."
                            else if (currentNumberString.length() > 13 && !(previousNumberString.length() > 10)) {
                                currentCalculationsString = previousNumberString + " × ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If only one of the numbers are over the display limit but the other is over the limit if we include the added "...", convert them both into "..." anyways
                            else {
                                currentCalculationsString = "... × ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the string is displayable, set the current calculations string as the numbers entered
                        else {
                            currentCalculationsString = previousNumberString + " × " + currentNumberString + " =";
                        }

                        break;
                    case "Divide":
                        previousNumber /= currentNumber;

                        // Sets the current calculations display
                        // If the combination of both inputs is larger than the display can handle, convert the smallest into "..." if the other is not too large
                        if (((previousNumberString.length() + currentNumberString.length()) > 13) && !((previousNumberString.length() > 10)) && (currentNumberString.length() > 10)) {
                            // If previousNumberString is the largest, convert it into "..."
                            if (previousNumberString.length() > currentNumberString.length()) {
                                currentCalculationsString = previousNumberString + " ÷ ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the largest, convert it into "..."
                            else if (currentNumberString.length() > previousNumberString.length()) {
                                currentCalculationsString = "... ÷ " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If they are the same length, convert previousNumberString by default
                            else {
                                currentCalculationsString = previousNumberString + " + ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the combination of both inputs is larger than the display can handle and either of the strings singlehandedly exceed the display, convert any that do into "..."
                        else if (previousNumberString.length() > 10 || currentNumberString.length() > 10) {
                            // If both exceed the limit, convert them both into "..."
                            if (previousNumberString.length() > 13 && currentNumberString.length() > 13) {
                                currentCalculationsString = "... ÷ ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                            // If previousNumberString is the one that exceeds the limit, convert it into "...", but only if the currentNumberString doesn't exceed the limit after adding "..."
                            else if (previousNumberString.length() > 13 && !(currentNumberString.length() > 10)) {
                                currentCalculationsString = "... ÷ " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the one that exceeds the limit, convert it into "...", but only if the previousNumberString doesn't exceed the limit after adding "..."
                            else if (currentNumberString.length() > 13 && !(previousNumberString.length() > 10)) {
                                currentCalculationsString = previousNumberString + " ÷ ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If only one of the numbers are over the display limit but the other is over the limit if we include the added "...", convert them both into "..." anyways
                            else {
                                currentCalculationsString = "... ÷ ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the string is displayable, set the current calculations string as the numbers entered
                        else {
                            currentCalculationsString = previousNumberString + " ÷ " + currentNumberString + " =";
                        }

                        break;
                }

                // Replace the current number with the results of the pending operation
                currentNumberString = Double.valueOf(previousNumber).toString();

                // If the current string ends with a decimal and zero, remove it
                if (currentNumberString.endsWith(".0")) {
                    currentNumberString = currentNumberString.replace(".0", "");
                }

                // Set the display number as the current number
                displayTextView.setText(currentNumberString);
                System.out.println("Display is " + currentNumberString);

                // Set the current calculations display to the entered operation
                currentCalculationsTextView.setText(currentCalculationsString);
                System.out.println("Current calculations are " + currentCalculationsString);
            }
            // If there was a pending operation but the user has not input another number, release the old pending operation
            else if (pendingOperation != null) {
                // Release the pressed operation button
                if (pendingOperationButton != null) {
                    pendingOperationButton.setVisibility(View.VISIBLE);
                    pendingOperationButtonText.setVisibility(View.VISIBLE);
                    System.out.println(pendingOperationButton.toString() + " and its text made visible");
                }
            }

            // Save the current number as the previous number
            previousNumber = Double.parseDouble(currentNumberString);

            // Set the previous number string to the current number for future reference
            previousNumberString = currentNumberString;

            // Set the pending operation as the selected operation
            pendingOperation = "Add";

            // Sets the current pressed operation as the selected operation
            pendingOperationButton = additionButton;
            pendingOperationButtonText = additionButtonText;
            pendingOperationButton.setVisibility(View.INVISIBLE);
            pendingOperationButtonText.setVisibility(View.INVISIBLE);

            // Clear the display on the next digit entered
            clearDisplayNextDigit = true;

            System.out.println("Addition was tapped");
        });

        equalsButton.setOnClickListener(v -> {

            // If there is a pending operation
            if (pendingOperation != null && !clearDisplayNextDigit) {
                // Convert current number to a double
                double currentNumber = Double.parseDouble(currentNumberString);

                // Perform the pending operation on the number if it exists
                switch (pendingOperation) {
                    case "Add":
                        previousNumber += currentNumber;

                        // Set the current calculations display
                        // If the combination of both inputs is larger than the display can handle, convert the smallest into "..." if the other is not too large
                        if (((previousNumberString.length() + currentNumberString.length()) > 13) && !((previousNumberString.length() > 10)) && (currentNumberString.length() > 10)) {
                            // If previousNumberString is the largest, convert it into "..."
                            if (previousNumberString.length() > currentNumberString.length()) {
                                currentCalculationsString = previousNumberString + " + ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the largest, convert it into "..."
                            else if (currentNumberString.length() > previousNumberString.length()) {
                                currentCalculationsString = "... + " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If they are the same length, convert previousNumberString by default
                            else {
                                currentCalculationsString = previousNumberString + " + ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the combination of both inputs is larger than the display can handle and either of the strings singlehandedly exceed the display, convert any that do into "..."
                        else if (previousNumberString.length() > 10 || currentNumberString.length() > 10) {
                            // If both exceed the limit, convert them both into "..."
                            if (previousNumberString.length() > 13 && currentNumberString.length() > 13) {
                                currentCalculationsString = "... + ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                            // If previousNumberString is the one that exceeds the limit, convert it into "...", but only if the currentNumberString doesn't exceed the limit after adding "..."
                            else if (previousNumberString.length() > 13 && !(currentNumberString.length() > 10)) {
                                currentCalculationsString = "... + " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the one that exceeds the limit, convert it into "...", but only if the previousNumberString doesn't exceed the limit after adding "..."
                            else if (currentNumberString.length() > 13 && !(previousNumberString.length() > 10)) {
                                currentCalculationsString = previousNumberString + " + ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If only one of the numbers are over the display limit but the other is over the limit if we include the added "...", convert them both into "..." anyways
                            else {
                                currentCalculationsString = "... + ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the string is displayable, set the current calculations string as the numbers entered
                        else {
                            currentCalculationsString = previousNumberString + " + " + currentNumberString + " =";
                        }

                        break;
                    case "Subtract":
                        previousNumber -= currentNumber;

                        // Set the current calculations display
                        // If the combination of both inputs is larger than the display can handle, convert the smallest into "..." if the other is not too large
                        if (((previousNumberString.length() + currentNumberString.length()) > 13) && !((previousNumberString.length() > 10)) && (currentNumberString.length() > 10)) {
                            // If previousNumberString is the largest, convert it into "..."
                            if (previousNumberString.length() > currentNumberString.length()) {
                                currentCalculationsString = previousNumberString + " − ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the largest, convert it into "..."
                            else if (currentNumberString.length() > previousNumberString.length()) {
                                currentCalculationsString = "... − " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If they are the same length, convert previousNumberString by default
                            else {
                                currentCalculationsString = previousNumberString + " + ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the combination of both inputs is larger than the display can handle and either of the strings singlehandedly exceed the display, convert any that do into "..."
                        else if (previousNumberString.length() > 10 || currentNumberString.length() > 10) {
                            // If both exceed the limit, convert them both into "..."
                            if (previousNumberString.length() > 13 && currentNumberString.length() > 13) {
                                currentCalculationsString = "... − ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                            // If previousNumberString is the one that exceeds the limit, convert it into "...", but only if the currentNumberString doesn't exceed the limit after adding "..."
                            else if (previousNumberString.length() > 13 && !(currentNumberString.length() > 10)) {
                                currentCalculationsString = "... − " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the one that exceeds the limit, convert it into "...", but only if the previousNumberString doesn't exceed the limit after adding "..."
                            else if (currentNumberString.length() > 13 && !(previousNumberString.length() > 10)) {
                                currentCalculationsString = previousNumberString + " − ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If only one of the numbers are over the display limit but the other is over the limit if we include the added "...", convert them both into "..." anyways
                            else {
                                currentCalculationsString = "... − ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the string is displayable, set the current calculations string as the numbers entered
                        else {
                            currentCalculationsString = previousNumberString + " − " + currentNumberString + " =";
                        }

                        break;
                    case "Multiply":
                        previousNumber *= currentNumber;

                        // Set the current calculations display
                        // If the combination of both inputs is larger than the display can handle, convert the smallest into "..." if the other is not too large
                        if (((previousNumberString.length() + currentNumberString.length()) > 13) && !((previousNumberString.length() > 10)) && (currentNumberString.length() > 10)) {
                            // If previousNumberString is the largest, convert it into "..."
                            if (previousNumberString.length() > currentNumberString.length()) {
                                currentCalculationsString = previousNumberString + " × ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the largest, convert it into "..."
                            else if (currentNumberString.length() > previousNumberString.length()) {
                                currentCalculationsString = "... × " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If they are the same length, convert previousNumberString by default
                            else {
                                currentCalculationsString = previousNumberString + " + ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the combination of both inputs is larger than the display can handle and either of the strings singlehandedly exceed the display, convert any that do into "..."
                        else if (previousNumberString.length() > 10 || currentNumberString.length() > 10) {
                            // If both exceed the limit, convert them both into "..."
                            if (previousNumberString.length() > 13 && currentNumberString.length() > 13) {
                                currentCalculationsString = "... × ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                            // If previousNumberString is the one that exceeds the limit, convert it into "...", but only if the currentNumberString doesn't exceed the limit after adding "..."
                            else if (previousNumberString.length() > 13 && !(currentNumberString.length() > 10)) {
                                currentCalculationsString = "... × " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the one that exceeds the limit, convert it into "...", but only if the previousNumberString doesn't exceed the limit after adding "..."
                            else if (currentNumberString.length() > 13 && !(previousNumberString.length() > 10)) {
                                currentCalculationsString = previousNumberString + " × ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If only one of the numbers are over the display limit but the other is over the limit if we include the added "...", convert them both into "..." anyways
                            else {
                                currentCalculationsString = "... × ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the string is displayable, set the current calculations string as the numbers entered
                        else {
                            currentCalculationsString = previousNumberString + " × " + currentNumberString + " =";
                        }

                        break;
                    case "Divide":
                        previousNumber /= currentNumber;

                        // Set the current calculations display
                        // If the combination of both inputs is larger than the display can handle, convert the smallest into "..." if the other is not too large
                        if (((previousNumberString.length() + currentNumberString.length()) > 13) && !((previousNumberString.length() > 10)) && (currentNumberString.length() > 10)) {
                            // If previousNumberString is the largest, convert it into "..."
                            if (previousNumberString.length() > currentNumberString.length()) {
                                currentCalculationsString = previousNumberString + " ÷ ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the largest, convert it into "..."
                            else if (currentNumberString.length() > previousNumberString.length()) {
                                currentCalculationsString = "... ÷ " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If they are the same length, convert previousNumberString by default
                            else {
                                currentCalculationsString = previousNumberString + " + ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the combination of both inputs is larger than the display can handle and either of the strings singlehandedly exceed the display, convert any that do into "..."
                        else if (previousNumberString.length() > 10 || currentNumberString.length() > 10) {
                            // If both exceed the limit, convert them both into "..."
                            if (previousNumberString.length() > 13 && currentNumberString.length() > 13) {
                                currentCalculationsString = "... ÷ ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                            // If previousNumberString is the one that exceeds the limit, convert it into "...", but only if the currentNumberString doesn't exceed the limit after adding "..."
                            else if (previousNumberString.length() > 13 && !(currentNumberString.length() > 10)) {
                                currentCalculationsString = "... ÷ " + currentNumberString + " =";
                                System.out.println("Current Calculations Display: previousNumberString replaced with \"...\" to fit display");
                            }
                            // If currentNumberString is the one that exceeds the limit, convert it into "...", but only if the previousNumberString doesn't exceed the limit after adding "..."
                            else if (currentNumberString.length() > 13 && !(previousNumberString.length() > 10)) {
                                currentCalculationsString = previousNumberString + " ÷ ... =";
                                System.out.println("Current Calculations Display: currentNumberString replaced with \"...\" to fit display");
                            }
                            // If only one of the numbers are over the display limit but the other is over the limit if we include the added "...", convert them both into "..." anyways
                            else {
                                currentCalculationsString = "... ÷ ... =";
                                System.out.println("Current Calculations Display: previousNumberString and currentNumberString replaced with \"...\" to fit display");
                            }
                        }
                        // If the string is displayable, set the current calculations string as the numbers entered
                        else {
                            currentCalculationsString = previousNumberString + " ÷ " + currentNumberString + " =";
                        }

                        break;
                }

                // Replace the current number with the results of the pending operation
                currentNumberString = Double.valueOf(previousNumber).toString();

                // If the current string ends with a decimal and zero, remove it
                if (currentNumberString.endsWith(".0")) {
                    currentNumberString = currentNumberString.replace(".0", "");
                }

                // Set the display number as the current number
                displayTextView.setText(currentNumberString);
                System.out.println("Display is " + currentNumberString);

                // Set the current calculations display to the entered operation
                currentCalculationsTextView.setText(currentCalculationsString);
                System.out.println("Current calculations are " + currentCalculationsString);
            }
            // If there was a pending operation but the user has not input another number, release the old pending operation
            else if (pendingOperation != null) {
                // Release the pressed operation button
                if (pendingOperationButton != null) {
                    pendingOperationButton.setVisibility(View.VISIBLE);
                    pendingOperationButtonText.setVisibility(View.VISIBLE);
                    System.out.println(pendingOperationButton.toString() + " and its text made visible");
                }
            }

            // Clear the pending operation
            pendingOperation = null;

            // Clear the display on the next digit entered
            clearDisplayNextDigit = true;

            System.out.println("Equals was tapped");
        });

        nineButton.setOnClickListener(v -> {

            // Clears the display if the next digit entered is the first
            if (clearDisplayNextDigit) {
                currentNumberString = "0";
                clearDisplayNextDigit = false;
                currentNumberHasDecimal = false;

                // If there is no pending operation, reset the current calculations display
                if (pendingOperation == null) {
                    currentCalculationsString = "";
                    currentCalculationsTextView.setText(currentCalculationsString);
                    System.out.println("Current calculations are " + currentCalculationsString);
                }

                // If there is an operation, release the pressed operation button
                if (pendingOperationButton != null) {
                    pendingOperationButton.setVisibility(View.VISIBLE);
                    pendingOperationButtonText.setVisibility(View.VISIBLE);
                    System.out.println(pendingOperationButton.toString() + " and its text made visible");
                }
            }

            // Appends digit to the end of the current number
            final String enteredDigit = "9";
            if (currentNumberString.equals("0")) {
                // If the current number is only a zero, replace it with the digit
                currentNumberString = enteredDigit;
            }
            else {
                // If the current number is not only a zero, append the entered digit at the end of the current number
                currentNumberString += enteredDigit;
            }

            // Set the display number as the current number
            displayTextView.setText(currentNumberString);
            System.out.println("Display is " + currentNumberString);

            System.out.println("Nine was tapped");
        });

        eightButton.setOnClickListener(v -> {

            // Clears the display if the next digit entered is the first
            if (clearDisplayNextDigit) {
                currentNumberString = "0";
                clearDisplayNextDigit = false;
                currentNumberHasDecimal = false;

                // If there is no pending operation, reset the current calculations display
                if (pendingOperation == null) {
                    currentCalculationsString = "";
                    currentCalculationsTextView.setText(currentCalculationsString);
                    System.out.println("Current calculations are " + currentCalculationsString);
                }

                // If there is an operation, release the pressed operation button
                if (pendingOperationButton != null) {
                    pendingOperationButton.setVisibility(View.VISIBLE);
                    pendingOperationButtonText.setVisibility(View.VISIBLE);
                    System.out.println(pendingOperationButton.toString() + " and its text made visible");
                }
            }

            // Appends digit to the end of the current number
            final String enteredDigit = "8";
            if (currentNumberString.equals("0")) {
                // If the current number is only a zero, replace it with the digit
                currentNumberString = enteredDigit;
            }
            else {
                // If the current number is not only a zero, append the entered digit at the end of the current number
                currentNumberString += enteredDigit;
            }

            // Set the display number as the current number
            displayTextView.setText(currentNumberString);
            System.out.println("Display is " + currentNumberString);

            System.out.println("Eight was tapped");
        });

        sevenButton.setOnClickListener(v -> {

            // Clears the display if the next digit entered is the first
            if (clearDisplayNextDigit) {
                currentNumberString = "0";
                clearDisplayNextDigit = false;
                currentNumberHasDecimal = false;

                // If there is no pending operation, reset the current calculations display
                if (pendingOperation == null) {
                    currentCalculationsString = "";
                    currentCalculationsTextView.setText(currentCalculationsString);
                    System.out.println("Current calculations are " + currentCalculationsString);
                }

                // If there is an operation, release the pressed operation button
                if (pendingOperationButton != null) {
                    pendingOperationButton.setVisibility(View.VISIBLE);
                    pendingOperationButtonText.setVisibility(View.VISIBLE);
                    System.out.println(pendingOperationButton.toString() + " and its text made visible");
                }
            }

            // Appends digit to the end of the current number
            final String enteredDigit = "7";
            if (currentNumberString.equals("0")) {
                // If the current number is only a zero, replace it with the digit
                currentNumberString = enteredDigit;
            }
            else {
                // If the current number is not only a zero, append the entered digit at the end of the current number
                currentNumberString += enteredDigit;
            }

            // Set the display number as the current number
            displayTextView.setText(currentNumberString);
            System.out.println("Display is " + currentNumberString);

            System.out.println("Seven was tapped");
        });

        sixButton.setOnClickListener(v -> {

            // Clears the display if the next digit entered is the first
            if (clearDisplayNextDigit) {
                currentNumberString = "0";
                clearDisplayNextDigit = false;
                currentNumberHasDecimal = false;

                // If there is no pending operation, reset the current calculations display
                if (pendingOperation == null) {
                    currentCalculationsString = "";
                    currentCalculationsTextView.setText(currentCalculationsString);
                    System.out.println("Current calculations are " + currentCalculationsString);
                }

                // If there is an operation, release the pressed operation button
                if (pendingOperationButton != null) {
                    pendingOperationButton.setVisibility(View.VISIBLE);
                    pendingOperationButtonText.setVisibility(View.VISIBLE);
                    System.out.println(pendingOperationButton.toString() + " and its text made visible");
                }
            }

            // Appends digit to the end of the current number
            final String enteredDigit = "6";
            if (currentNumberString.equals("0")) {
                // If the current number is only a zero, replace it with the digit
                currentNumberString = enteredDigit;
            }
            else {
                // If the current number is not only a zero, append the entered digit at the end of the current number
                currentNumberString += enteredDigit;
            }

            displayTextView.setText(currentNumberString);
            System.out.println("Display is " + currentNumberString);

            System.out.println("Six was tapped");
        });

        fiveButton.setOnClickListener(v -> {

            // Clears the display if the next digit entered is the first
            if (clearDisplayNextDigit) {
                currentNumberString = "0";
                clearDisplayNextDigit = false;
                currentNumberHasDecimal = false;

                // If there is no pending operation, reset the current calculations display
                if (pendingOperation == null) {
                    currentCalculationsString = "";
                    currentCalculationsTextView.setText(currentCalculationsString);
                    System.out.println("Current calculations are " + currentCalculationsString);
                }

                // If there is an operation, release the pressed operation button
                if (pendingOperationButton != null) {
                    pendingOperationButton.setVisibility(View.VISIBLE);
                    pendingOperationButtonText.setVisibility(View.VISIBLE);
                    System.out.println(pendingOperationButton.toString() + " and its text made visible");
                }
            }

            // Appends digit to the end of the current number
            final String enteredDigit = "5";
            if (currentNumberString.equals("0")) {
                // If the current number is only a zero, replace it with the digit
                currentNumberString = enteredDigit;
            }
            else {
                // If the current number is not only a zero, append the entered digit at the end of the current number
                currentNumberString += enteredDigit;
            }

            // Display the current number
            displayTextView.setText(currentNumberString);
            System.out.println("Display is " + currentNumberString);

            System.out.println("Five was tapped");
        });

        fourButton.setOnClickListener(v -> {

            // Clears the display if the next digit entered is the first
            if (clearDisplayNextDigit) {
                currentNumberString = "0";
                clearDisplayNextDigit = false;
                currentNumberHasDecimal = false;

                // If there is no pending operation, reset the current calculations display
                if (pendingOperation == null) {
                    currentCalculationsString = "";
                    currentCalculationsTextView.setText(currentCalculationsString);
                    System.out.println("Current calculations are " + currentCalculationsString);
                }

                // If there is an operation, release the pressed operation button
                if (pendingOperationButton != null) {
                    pendingOperationButton.setVisibility(View.VISIBLE);
                    pendingOperationButtonText.setVisibility(View.VISIBLE);
                    System.out.println(pendingOperationButton.toString() + " and its text made visible");
                }
            }

            // Appends digit to the end of the current number
            final String enteredDigit = "4";
            if (currentNumberString.equals("0")) {
                // If the current number is only a zero, replace it with the digit
                currentNumberString = enteredDigit;
            }
            else {
                // If the current number is not only a zero, append the entered digit at the end of the current number
                currentNumberString += enteredDigit;
            }

            // Set the display number as the current number
            displayTextView.setText(currentNumberString);
            System.out.println("Display is " + currentNumberString);

            System.out.println("Four was tapped");
        });

        threeButton.setOnClickListener(v -> {

            // Clears the display if the next digit entered is the first
            if (clearDisplayNextDigit) {
                currentNumberString = "0";
                clearDisplayNextDigit = false;
                currentNumberHasDecimal = false;

                // If there is no pending operation, reset the current calculations display
                if (pendingOperation == null) {
                    currentCalculationsString = "";
                    currentCalculationsTextView.setText(currentCalculationsString);
                    System.out.println("Current calculations are " + currentCalculationsString);
                }

                // If there is an operation, release the pressed operation button
                if (pendingOperationButton != null) {
                    pendingOperationButton.setVisibility(View.VISIBLE);
                    pendingOperationButtonText.setVisibility(View.VISIBLE);
                    System.out.println(pendingOperationButton.toString() + " and its text made visible");
                }
            }

            // Appends digit to the end of the current number
            final String enteredDigit = "3";
            if (currentNumberString.equals("0")) {
                // If the current number is only a zero, replace it with the digit
                currentNumberString = enteredDigit;
            }
            else {
                // If the current number is not only a zero, append the entered digit at the end of the current number
                currentNumberString += enteredDigit;
            }

            // Set the display number as the current number
            displayTextView.setText(currentNumberString);
            System.out.println("Display is " + currentNumberString);

            System.out.println("Three was tapped");
        });

        twoButton.setOnClickListener(v -> {

            // Clears the display if the next digit entered is the first
            if (clearDisplayNextDigit) {
                currentNumberString = "0";
                clearDisplayNextDigit = false;
                currentNumberHasDecimal = false;

                // If there is no pending operation, reset the current calculations display
                if (pendingOperation == null) {
                    currentCalculationsString = "";
                    currentCalculationsTextView.setText(currentCalculationsString);
                    System.out.println("Current calculations are " + currentCalculationsString);
                }

                // If there is an operation, release the pressed operation button
                if (pendingOperationButton != null) {
                    pendingOperationButton.setVisibility(View.VISIBLE);
                    pendingOperationButtonText.setVisibility(View.VISIBLE);
                    System.out.println(pendingOperationButton.toString() + " and its text made visible");
                }
            }

            // Appends digit to the end of the current number
            final String enteredDigit = "2";
            if (currentNumberString.equals("0")) {
                // If the current number is only a zero, replace it with the digit
                currentNumberString = enteredDigit;
            }
            else {
                // If the current number is not only a zero, append the entered digit at the end of the current number
                currentNumberString += enteredDigit;
            }

            // Set the display number as the current number
            displayTextView.setText(currentNumberString);
            System.out.println("Display is " + currentNumberString);

            System.out.println("Two was tapped");
        });

        oneButton.setOnClickListener(v -> {

            // Clears the display if the next digit entered is the first
            if (clearDisplayNextDigit) {
                currentNumberString = "0";
                clearDisplayNextDigit = false;
                currentNumberHasDecimal = false;

                // If there is no pending operation, reset the current calculations display
                if (pendingOperation == null) {
                    currentCalculationsString = "";
                    currentCalculationsTextView.setText(currentCalculationsString);
                    System.out.println("Current calculations are " + currentCalculationsString);
                }

                // If there is an operation, release the pressed operation button
                if (pendingOperationButton != null) {
                    pendingOperationButton.setVisibility(View.VISIBLE);
                    pendingOperationButtonText.setVisibility(View.VISIBLE);
                    System.out.println(pendingOperationButton.toString() + " and its text made visible");
                }
            }

            // Appends digit to the end of the current number
            final String enteredDigit = "1";
            if (currentNumberString.equals("0")) {
                // If the current number is only a zero, replace it with the digit
                currentNumberString = enteredDigit;
            }
            else {
                // If the current number is not only a zero, append the entered digit at the end of the current number
                currentNumberString += enteredDigit;
            }

            // Set the display number as the current number
            displayTextView.setText(currentNumberString);
            System.out.println("Display is " + currentNumberString);

            System.out.println("One was tapped");
        });

        zeroButton.setOnClickListener(v -> {

            // Clears the display if the next digit entered is the first
            if (clearDisplayNextDigit) {
                currentNumberString = "0";
                clearDisplayNextDigit = false;
                currentNumberHasDecimal = false;

                // If there is no pending operation, reset the current calculations display
                if (pendingOperation == null) {
                    currentCalculationsString = "";
                    currentCalculationsTextView.setText(currentCalculationsString);
                    System.out.println("Current calculations are " + currentCalculationsString);
                }

                // If there is an operation, release the pressed operation button
                if (pendingOperationButton != null) {
                    pendingOperationButton.setVisibility(View.VISIBLE);
                    pendingOperationButtonText.setVisibility(View.VISIBLE);
                    System.out.println(pendingOperationButton.toString() + " and its text made visible");
                }
            }

            // Appends digit to the end of the current number
            final String enteredDigit = "0";
            if (!currentNumberString.equals("0")) {
                // If the current number is not only a zero, append the entered digit at the end of the current number
                currentNumberString += enteredDigit;
            }

            // Set the display number as the current number
            displayTextView.setText(currentNumberString);
            System.out.println("Display is " + currentNumberString);

            System.out.println("Zero was tapped");
        });

        decimalButton.setOnClickListener(v -> {

            // If the number does not have a decimal yet, append a decimal when tapped
            if (!currentNumberHasDecimal) {
                // If the display needs to be cleared, append the decimal to a zero
                if (clearDisplayNextDigit) {
                    currentNumberString = "0.";
                    clearDisplayNextDigit = false;

                    // If there is an operation, release the pressed operation button
                    if (pendingOperationButton != null) {
                        pendingOperationButton.setVisibility(View.VISIBLE);
                        pendingOperationButtonText.setVisibility(View.VISIBLE);
                        System.out.println(pendingOperationButton.toString() + " and its text made visible");
                    }
                }
                else {
                    currentNumberString += ".";
                }

                currentNumberHasDecimal = true;
            }

            // Set the display number as the current number
            displayTextView.setText(currentNumberString);
            System.out.println("Display is " + currentNumberString);

            System.out.println("Decimal was tapped");
        });
    }
}